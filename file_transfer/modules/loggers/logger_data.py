from datetime import datetime
from enum import Enum


class LoggerLevel(str, Enum):
    INFO = "INFO"
    ERROR = "ERROR"
    WARNING = "WARNING"
    DEBUG = "DEBUG"


class LoggerData:
    """
    Class for holding logging data
    """
    when: datetime
    how: LoggerLevel
    who: str
    what: str

    def __init__(self, when: datetime, how: LoggerLevel, who: str, what: str):
        """
        :param when: datetime of log event
        :param how: level of log event
        :param who: what is the source of event
        :param what: log message
        """
        self.when = when
        self.how = how
        self.who = who
        self.what = what
