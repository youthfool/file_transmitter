from logging import Logger
import logging.config

from file_transfer.modules.loggers.execution_logger import ExecutionLogger
from file_transfer.modules.loggers.logger_data import LoggerData, LoggerLevel


class LocalLogger(ExecutionLogger):
    """
    Logger for saving log data in filesystem locally
    """

    local_logger: Logger

    def __init__(self, **kwargs):
        super(LocalLogger, self).__init__()
        dict_log_config = {
            "version": 1,
            'disable_existing_loggers': False,
            "handlers": {
                "fileHandler_local": {
                    "class": "logging.handlers.RotatingFileHandler",
                    "formatter": "formatter_local",
                    "filename": kwargs['source'] + "data_transfer.log",
                    "maxBytes": 20000000,
                    "backupCount": 5
                }
            },
            "loggers": {
                "logger_local": {
                    "handlers": ["fileHandler_local"],
                    "level": "INFO",
                }
            },
            "formatters": {
                "formatter_local": {
                    "format": "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
                }
            }
        }
        logging.config.dictConfig(dict_log_config)
        self.local_logger = logging.getLogger("logger_local")

    def save_event_data(self, event: LoggerData) -> None:
        message = f"[{event.who}] - {event.what}"
        if event.how == LoggerLevel.INFO:
            self.local_logger.info(message)
        elif event.how == LoggerLevel.ERROR:
            self.local_logger.error(message)
        elif event.how == LoggerLevel.WARNING:
            self.local_logger.warning(message)
        elif event.how == LoggerLevel.DEBUG:
            self.local_logger.debug(message)
