from datetime import datetime
from file_transfer.modules.loggers.logger_data import LoggerData, LoggerLevel


class ExecutionLogger:
    """
    Base abstract class for logging execution process
    """

    def __init__(self):
        self.logger_history: list[LoggerData] = []

    def save_event_data(self, event: LoggerData) -> None:
        """
        Save event data need custom implementation for each logging style (write file, send somewhere or else)
        :param event: logger data to save
        :return: None
        """
        raise NotImplementedError()

    def log_event(self, level: LoggerLevel, who: str, what: str) -> None:
        """
        Log some event
        :param level: level of log event
        :param who: source of log event
        :param what: log message, description of log event
        :return: None
        """
        when = datetime.now()
        event = LoggerData(when, level, who, what)
        self.logger_history.append(event)
        self.save_event_data(event)

    def log_error(self, who: str, what: str) -> None:
        """
        Log error
        :param who: source of log event
        :param what: description of log event
        :return: None
        """
        self.log_event(LoggerLevel.ERROR, who, what)

    def log_info(self, who: str, what: str) -> None:
        """
        Log information
        :param who: source of log event
        :param what: description of log event
        :return: None
        """
        self.log_event(LoggerLevel.INFO, who, what)

    def log_warning(self, who: str, what: str) -> None:
        """
        Log warning
        :param who: source of log event
        :param what: description of log event
        :return: None
        """
        self.log_event(LoggerLevel.WARNING, who, what)

    def log_debug(self, who: str, what: str) -> None:
        """
        Log debug information
        :param who: source of log event
        :param what: description of log event
        :return: None
        """
        self.log_event(LoggerLevel.DEBUG, who, what)
