from typing import List, Callable, Tuple
from os import listdir, stat
from os.path import isfile, join
import ftplib
from io import BytesIO
from file_transfer.tools.custom_decorators import repeat_on_error_decorator


# max supported file size in MB to transfer
TRANSFER_MAX_FILE_SIZE: int = 50


class BaseTransferProvider:
    """
    Base class to handle file transfer operations with common interface
    """

    def __init__(self, working_directory: str):
        self._working_directory = working_directory

    @property
    def working_directory(self):
        return self._working_directory

    @working_directory.setter
    def working_directory(self, value: str):
        self._working_directory = value

    def get_files_list(self, filter_func: Callable[[str], bool] = lambda string: True) -> List[str]:
        """
        Get files list from working directory with filter function
        :param filter_func: function to filter files
        :return: list of filenames
        """
        raise NotImplementedError()

    def get_file_string_list(self, filename: str) -> List[str]:
        """
        Get lines list from file in working directory
        :param filename: name of the file to get lines from
        :return: list of file lines
        """
        raise NotImplementedError()

    def send_file_by_lines(self, filename: str, lines: List[str]) -> None:
        """
        Transfer lines into file in working directory
        :param filename: name of file to create from lines
        :param lines: lines of file
        :return: None
        """
        raise NotImplementedError()

    def send_files(self, source_transfer_provider,
                   filenames: List[str],
                   file_modifier: Callable[[List[str]], List[str]] = lambda string_list: string_list,
                   filename_modifier: Callable[[str], str] = lambda string: string,) -> List[Tuple[str, Exception]]:
        """
        Handle common operation of getting file from one transfer provider and transfer it into
        another transfer provider
        :param source_transfer_provider: transfer provider to get files from
        :param filenames: names of files to get from source transfer provider
        :param file_modifier: function to make some modifications to file before transfer
        :param filename_modifier: function to make some modifications to filename before transfer
        :return: None
        """
        # not send files list with reason
        failed = []
        for filename in filenames:
            try:
                lines = source_transfer_provider.get_file_string_list(filename)
                lines = file_modifier(lines)
                new_filename = filename_modifier(filename)
                self.send_file_by_lines(new_filename, lines)
            except Exception as exc:
                failed.append((filename, exc))
        return failed


class LocalTransferProvider(BaseTransferProvider):
    """
    Transfer provider to handle operations on local file sources
    """

    def __init__(self, working_directory: str):
        BaseTransferProvider.__init__(self, working_directory)

    def get_files_list(self, filter_func: Callable[[str], bool] = lambda string: True) -> List[str]:
        return [f for f in listdir(self.working_directory) if
                isfile(join(self.working_directory, f)) and
                stat(join(self.working_directory, f)).st_size / 1024 / 1024 < TRANSFER_MAX_FILE_SIZE and
                filter_func(f)]

    def get_file_string_list(self, filename: str) -> List[str]:
        with open(join(self.working_directory, filename), 'r') as read_f:
            strings = read_f.readlines()
        return strings

    def send_file_by_lines(self, filename: str, lines: List[str]) -> None:
        with open(join(self.working_directory, filename), 'w') as write_f:
            write_f.writelines(lines)
            write_f.flush()


class FtpTransferProvider(BaseTransferProvider):
    """
    Transfer provider to handle operations with ftp source
    """

    def __init__(self, working_directory: str, server_address: str, login: str, password: str):
        BaseTransferProvider.__init__(self, working_directory)
        self._server_address = server_address
        self._login = login
        self._password = password

    @property
    def server_address(self):
        return self._server_address

    @server_address.setter
    def server_address(self, value: str):
        self._server_address = value

    @property
    def login(self):
        return self._login

    @login.setter
    def login(self, value: str):
        self._login = value

    @property
    def password(self):
        return self._password

    @password.setter
    def password(self, value: str):
        self._password = value

    @repeat_on_error_decorator()
    def get_files_list(self, filter_func: Callable[[str], bool] = lambda string: True) -> List[str]:
        with ftplib.FTP(host=self.server_address, user=self.login, passwd=self.password) as ftp:
            return [f[0] for f in ftp.mlsd(self.working_directory)
                    if f[1]['type'] == 'file' and
                    int(f[1]['size']) / 1024 / 1024 < TRANSFER_MAX_FILE_SIZE and
                    filter_func(f[0])]

    @repeat_on_error_decorator()
    def get_file_string_list(self, filename: str) -> List[str]:
        with ftplib.FTP(host=self.server_address, user=self.login, passwd=self.password) as ftp:
            bio = BytesIO()
            ftp.retrbinary('RETR ' + self.working_directory + filename, bio.write)
            strings = bio.getvalue().decode().splitlines(True)
            return strings

    @repeat_on_error_decorator()
    def send_file_by_lines(self, filename: str, lines: List[str]) -> None:
        with ftplib.FTP(host=self.server_address, user=self.login, passwd=self.password) as ftp:
            bio = BytesIO()
            bio.write(''.join(lines).encode())
            bio.seek(0)
            ftp.storlines('STOR ' + self.working_directory + filename, bio)
