from file_transfer.modules.configurations.configurations_provider import ConfigurationsProvider
import json
import pathlib

from file_transfer.modules.loggers import ExecutionLogger


class LocalJsonConfigProvider(ConfigurationsProvider):
    """
    Configurations provider, that loads json file with configurations from local filesystem
    """
    config_path: str
    configurations: list[dict] = []
    loaded: bool = False
    logger: ExecutionLogger

    def __init__(self, **kwargs):
        super(ConfigurationsProvider, self).__init__()
        self.config_path = kwargs['source']
        self.logger = kwargs['logger']

    def load_config(self) -> None:
        file_path = pathlib.Path(self.config_path)
        if file_path.exists():
            if file_path.is_file():
                self.configurations = self.load_config_file(self.config_path)
            else:
                files = [f for f in file_path.glob("*.json")
                         if not f.name.endswith("__example.json")]
                for file in files:
                    self.configurations += self.load_config_file(file)
        else:
            self.logger.log_error(__name__, f'No task config file or directory {self.config_path}')
        # set desirable format list[dict]
        self.configurations = [c for c in self.configurations if c]
        self.loaded = True

    def load_config_file(self, file: str | pathlib.Path) -> list[dict]:
        """
        Load single configurations file according to its format and containing information
        :param file:
        :return:
        """
        with open(file, 'r') as config_file:
            config = json.load(config_file)
            if 'tasks' in config and isinstance(config['tasks'], list) and isinstance(config['tasks'][0], dict):
                self.logger.log_info(__name__, f'Successful tasks configurations file {file} loading')
                return config['tasks']
            elif isinstance(config, dict):
                self.logger.log_info(__name__, f'Successful tasks configurations file {file} loading')
                return [config]
            else:
                # log error
                self.logger.log_error(__name__, f'Incorrect tasks configurations file {file} structure')

    def get_tasks_config_list(self) -> list[dict]:
        if not self.loaded:
            self.load_config()
        return self.configurations
