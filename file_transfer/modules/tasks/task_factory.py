from file_transfer.modules.loggers import ExecutionLogger
from file_transfer.modules.tasks.concat_file_transfer_task import ConcatFileTransferTask
from file_transfer.modules.tasks.file_transfer_task import FileTransferTask
from file_transfer.modules.tasks.transfer_task import TransferTask
from enum import Enum


class TransferTaskType(str, Enum):
    """
    Transfer tasks types
    """
    PUBLIC_CGGTTS = "CGGTTS format files publication task"
    UTCKM_LOC_DEF = "Difference between local coordinated and working scales"
    UTCKM_CLOCKS_CORRECTION = "Clock correction relative to UTCKm"
    ERP_RINEX = "Earth rotation parameters RINEX format files"
    ERP_TIME = "Earth rotation parameters TIME format files"


class TransferTaskFactory:
    """
    Factory class for creating Transfer Tasks
    """

    @staticmethod
    def create_task(task_config: dict, logger: ExecutionLogger) -> TransferTask | None:
        """
        Factory method creates task
        :param logger: logger reference
        :param task_config: configuration dictionary
        :return: transfer task instance
        """
        task: TransferTask | None = None

        if task_config['type'] == TransferTaskType.PUBLIC_CGGTTS.name:
            task = FileTransferTask(logger=logger, **task_config)
        elif task_config['type'] == TransferTaskType.UTCKM_LOC_DEF.name:
            task = ConcatFileTransferTask(logger=logger, **task_config)
        elif task_config['type'] == TransferTaskType.UTCKM_CLOCKS_CORRECTION.name:
            task = FileTransferTask(logger=logger, **task_config)
        elif task_config['type'] == TransferTaskType.ERP_RINEX.name:
            pass
        elif task_config['type'] == TransferTaskType.ERP_TIME.name:
            pass

        return task
