import re
from datetime import datetime, timedelta
from file_transfer.modules.tasks.filters.base_filter import BaseFilter
from file_transfer.tools.util import set_attributes


class DefFileFilter(BaseFilter):
    """
    Class to determine filtration rule for def task source files
    """
    def __init__(self, **kwargs):
        BaseFilter.__init__(self)
        self.pattern = ''
        self.period = 0
        set_attributes(self, **kwargs)
        self.pattern = re.compile(self.pattern)
        self.current_datetime = datetime.utcnow()

    def filter(self, parameter: str) -> bool:
        dt = datetime.strptime(parameter[-13:-5], '%Y%m%d')
        return self.pattern.match(parameter) and \
            dt >= self.current_datetime - timedelta(days=self.period)
