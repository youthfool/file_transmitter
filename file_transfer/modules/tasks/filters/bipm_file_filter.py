import re
from datetime import datetime
from file_transfer.modules.tasks.filters.base_filter import BaseFilter
from file_transfer.tools.util import get_mjd_datetime, set_attributes


class BIPMFileFilter(BaseFilter):
    """
    Class to determine filtration rule for BIPM files
    """
    def __init__(self, **kwargs):
        BaseFilter.__init__(self)
        self.pattern = ''
        self.period = 0
        set_attributes(self, **kwargs)
        self.pattern = re.compile(self.pattern)
        self.current_mjd = get_mjd_datetime(datetime.utcnow())

    def filter(self, parameter: str) -> bool:
        return self.pattern.match(parameter) and \
            int(parameter[-6:-4] + parameter[-3:]) >= self.current_mjd - self.period
