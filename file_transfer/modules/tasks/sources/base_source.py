from typing import Callable


class BaseSource:
    """
    Base data source class
    """

    def __init__(self):
        self.address = ""
        self.server = ""
        self.login = ""
        self.password = ""

    def get_data_objects_list(self, address: str,
                              filter_func: Callable[[str], bool] = lambda string: True) -> list[str]:
        """
        Get list of data objects (filenames, links or something else that contains needed data)
        :param address: address of data objects
        :param filter_func: function to filter data objects
        :return: list of data objects on the source
        """
        raise NotImplementedError()

    def get_data_objects(self, address: str, data_objects_list: list[str], temp_path: str) -> None:
        """
        Get data objects from source and store it in temporary location
        :param address: address of data objects
        :param temp_path: temporary path to store data objects
        :param data_objects_list: list of filenames or links or something else that contains data
        :return:
        """
        raise NotImplementedError()

    def get_data_from_data_objects(self, address: str, data_object: str) -> list[str]:
        """
        Get data as strings from data objects
        :param address: address of data objects to get data from
        :param data_object: data object in string notation (file path, link or something else containing data)
        :return: list of data in string notation
        """
        raise NotImplementedError()

    def send_data(self, address: str, data_object: str, data: list[str]) -> None:
        """
        Send data to the source
        :param address: address of data object to save data in
        :param data_object: data object (filename, link to send data to)
        :param data: data to send
        :return: None
        """
        raise NotImplementedError()

    def send_data_objects(self, address: str, data_objects_list: list[str], temp_path: str) -> None:
        """
        Send data objects from temporary location to address
        :param address: address where to save data objects
        :param data_objects_list: list of filenames or links or something else that contains data
        :param temp_path: temporary path to get data objects from
        :return: None
        """
        raise NotImplementedError()
