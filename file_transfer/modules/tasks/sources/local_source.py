from os import listdir, stat
from os.path import isfile, join
from typing import Callable

from file_transfer.modules.tasks.sources.base_source import BaseSource
from file_transfer.tools.util import set_attributes


# max supported file size in MB to transfer
MAX_TRANSFER_FILE_SIZE: int = 50


class LocalFileSource(BaseSource):
    """
    Local source provider handles source operations for filesystem
    """

    def __init__(self, **kwargs):
        super(LocalFileSource, self).__init__()
        set_attributes(self, **kwargs)

    def get_data_objects_list(self, address: str,
                              filter_func: Callable[[str], bool] = lambda string: True) -> list[str]:
        return [f for f in listdir(address) if
                isfile(join(address, f)) and
                stat(join(address, f)).st_size / 1024 / 1024 < MAX_TRANSFER_FILE_SIZE and
                filter_func(f)]

    def get_data_objects(self, address: str, data_objects_list: list[str], temp_path: str) -> None:
        for data_object in data_objects_list:
            with open(join(address, data_object), 'rb') as read_file:
                with open(join(temp_path, data_object), 'wb') as write_file:
                    write_file.write(read_file.read())
                    write_file.flush()

    def get_data_from_data_objects(self, address: str, data_object: str) -> list[str]:
        with open(join(address, data_object), 'r') as read_f:
            strings = read_f.readlines()
        return strings

    def send_data(self, address: str, data_object: str, data: list[str]) -> None:
        with open(join(address, data_object), 'w') as write_file:
            write_file.writelines(data)
            write_file.flush()

    def send_data_objects(self, address: str, data_objects_list: list[str], temp_path: str) -> None:
        for data_object in data_objects_list:
            with open(join(temp_path, data_object), 'rb') as read_file:
                with open(join(address, data_object), 'wb') as write_file:
                    write_file.write(read_file.read())
                    write_file.flush()
