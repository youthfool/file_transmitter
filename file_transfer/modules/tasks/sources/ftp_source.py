import ftplib
from io import BytesIO
from typing import Callable
from os.path import join

from file_transfer.modules.tasks.sources.base_source import BaseSource
from file_transfer.tools.custom_decorators import repeat_on_error_decorator
from file_transfer.tools.util import set_attributes


# max supported file size in MB to transfer
MAX_TRANSFER_FILE_SIZE: int = 50


class FtpFileSource(BaseSource):
    """
    FTP server source provider
    """

    def __init__(self, **kwargs):
        super(FtpFileSource, self).__init__()
        set_attributes(self, **kwargs)

    @repeat_on_error_decorator()
    def get_data_objects_list(self, address: str,
                              filter_func: Callable[[str], bool] = lambda string: True) -> list[str]:
        with ftplib.FTP(host=self.server, user=self.login, passwd=self.password) as ftp:
            return [f[0] for f in ftp.mlsd(address)
                    if f[1]['type'] == 'file' and
                    int(f[1]['size']) / 1024 / 1024 < MAX_TRANSFER_FILE_SIZE and
                    filter_func(f[0])]

    def get_data_objects(self, address: str, data_objects_list: list[str], temp_path: str) -> None:
        for data_object in data_objects_list:
            # because of ftp problems, it is easier to open ftp connection for each single file
            bio: BytesIO = self.get_data_object(address, data_object)
            with open(join(temp_path, data_object), 'wb') as write_file:
                write_file.write(bio.getvalue())

    @repeat_on_error_decorator()
    def get_data_object(self, address: str, data_object: str) -> BytesIO:
        """
        Get single file from ftp
        :param address: address of file on ftp server
        :param data_object: name of the data file
        :return: BytesIO of the file
        """
        with ftplib.FTP(host=self.server, user=self.login, passwd=self.password) as ftp:
            bio = BytesIO()
            ftp.retrbinary('RETR ' + join(address, data_object), bio.write)
            return bio

    @repeat_on_error_decorator()
    def get_data_from_data_objects(self, address: str, data_object: str) -> list[str]:
        with ftplib.FTP(host=self.server, user=self.login, passwd=self.password) as ftp:
            bio = BytesIO()
            ftp.retrbinary('RETR ' + join(address, data_object), bio.write)
            strings = bio.getvalue().decode().splitlines(True)
            return strings

    @repeat_on_error_decorator()
    def send_data(self, address: str, data_object: str, data: list[str]) -> None:
        with ftplib.FTP(host=self.server, user=self.login, passwd=self.password) as ftp:
            bio = BytesIO()
            bio.write(''.join(data).encode())
            bio.seek(0)
            ftp.storlines('STOR ' + join(address, data_object), bio)

    def send_data_objects(self, address: str, data_objects_list: list[str], temp_path: str) -> None:
        for data_object in data_objects_list:
            with open(join(temp_path, data_object), 'rb') as read_file:
                bio = BytesIO()
                bio.write(read_file.read())
                bio.seek(0)
                # because of ftp problems, it is easier to open ftp connection for each single file
                self.send_data_object(address, data_object, bio)

    @repeat_on_error_decorator()
    def send_data_object(self, address: str, data_object: str, bio: BytesIO) -> None:
        """
        Send single data object to ftp
        :param bio: BytesIO with data of file to store on ftp
        :param address: address on ftp to send data object to
        :param data_object: name of the file on ftp to store data to
        :return: None
        """
        with ftplib.FTP(host=self.server, user=self.login, passwd=self.password) as ftp:
            ftp.storlines('STOR ' + join(address, data_object), bio)
