from file_transfer.modules.tasks.formatters.base_formatter import BaseFormatter
from file_transfer.tools.util import get_mjd_datetime, get_datetime_mjd
from datetime import datetime, timedelta


class DefFormatter(BaseFormatter):
    """
    Definitive file name formatter class
    """
    def __init__(self, **kwargs):
        super(DefFormatter, self).__init__()

    def apply(self, base_objects: list[str]) -> list[str]:
        result_list: list[str] = []
        mjds = {}
        for file_name in base_objects:
            if file_name.endswith('.stat'):
                mjd = get_mjd_datetime(datetime.strptime(file_name[-13:-5], '%Y%m%d'))
                mjd_key = mjd + (4 - mjd % 5)
                if mjd_key in mjds:
                    mjds[mjd_key].append(f'UTC(Km)_LOC_{mjd_key}_def.dat')
                else:
                    mjds[mjd_key] = [f'UTC(Km)_LOC_{mjd_key}_def.dat']
            elif file_name.endswith('_def.dat'):
                mjd = int(file_name[-13:-8])
                dt = get_datetime_mjd(mjd)
                for i in range(5):
                    dt_i = dt - timedelta(days=i)
                    if mjd in mjds:
                        mjds[mjd].append(f'KFShV_UTC(Lab)_{dt_i.strftime("%Y%m%d")}.stat')
                    else:
                        mjds[mjd] = [f'KFShV_UTC(Lab)_{dt_i.strftime("%Y%m%d")}.stat']
        for mjd in mjds:
            if len(mjds[mjd]) >= 5:
                result_list += mjds[mjd]
        return list(set(result_list))
