from file_transfer.modules.tasks.formatters.base_formatter import BaseFormatter


class BipmFormatter(BaseFormatter):
    """
    Bipm file name formatter class
    """
    def __init__(self, **kwargs):
        super(BipmFormatter, self).__init__()

    def apply(self, base_objects: list[str]) -> list[str]:
        result_list: list[str] = []

        for file_name in base_objects:
            if file_name.startswith('CDKm'):
                result_list.append(f'Km{file_name[-6:-4]}{file_name[-3:]}.clk')
            elif file_name.startswith('Km'):
                result_list.append(f'CDKm__{file_name[-9:-7]}.{file_name[-7:-4]}')
            else:
                result_list.append(file_name)

        return result_list
