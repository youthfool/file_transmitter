from file_transfer.modules.tasks.transfer_task import TransferTask


class FileTransferTask(TransferTask):
    """
    File transfer task
    """

    def __init__(self, **kwargs):
        super(FileTransferTask, self).__init__(**kwargs)

    def execute(self):
        try:
            # get source files list
            source_objects_list = self.source.get_data_objects_list(self.source.address, self.source_filter.filter)
        except Exception as err:
            self.logger.log_error(self.name, f'Source error {str(err)}')
            return
        try:
            # get destination files list
            destination_objects_list = self.destination.get_data_objects_list(self.destination.address)
        except Exception as err:
            self.logger.log_error(self.name, f'Destination error {str(err)}')
            return
        try:
            # get destination files list with file names formatted to source files names
            destination_objects_formatted = self.destination_formatter.apply(destination_objects_list)
        except Exception as err:
            self.logger.log_error(self.name, f'Formatter error {str(err)}')
            return

        print(destination_objects_formatted)
        print(source_objects_list)
        # keep source file names which are not in destination address
        source_objects_list = [file for file in source_objects_list if file not in destination_objects_formatted]

        for source_object in source_objects_list:
            try:
                # get data from source file
                data = self.source.get_data_from_data_objects(self.source.address, source_object)
                # apply modifiers to files data
                for modifier in self.modifiers:
                    data = modifier.modify(string_list=data)
                # get new file name formatted from source file name to destination file name
                new_object_name = self.destination_formatter.apply([source_object])[0]
                # send file to destination address
                self.destination.send_data(self.destination.address, new_object_name, data)
            except Exception as err:
                self.logger.log_error(self.name, f'{source_object} handling error {err}')
