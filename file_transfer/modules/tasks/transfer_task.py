from file_transfer.modules.loggers import ExecutionLogger
from file_transfer.modules.tasks.filters import *
from file_transfer.modules.tasks.formatters import *
from file_transfer.modules.tasks.modifiers import *
from file_transfer.modules.tasks.sources import *


class TransferTask:
    """
    Base abstract transfer task class
    """

    name: str
    source: BaseSource
    source_filter: BaseFilter
    source_formatter: BaseFormatter
    modifiers: list[BaseModifier]
    destination_formatter: BaseFormatter
    destination: BaseSource
    logger: ExecutionLogger

    def __init__(self, logger: ExecutionLogger, **kwargs):
        self.name = kwargs['name']
        self.logger = logger
        self.source = globals()[kwargs['source']['class']](**kwargs['source'])

        if 'filter' in kwargs and 'class' in kwargs['filter'] \
                and len(kwargs['filter']['class']):
            self.source_filter = globals()[kwargs['filter']['class']](**kwargs['filter'])
        else:
            self.source_filter = BaseFilter()

        if 'source_formatter' in kwargs and 'class' in kwargs['source_formatter'] \
                and len(kwargs['source_formatter']['class']):
            self.source_formatter = globals()[kwargs['source_formatter']['class']](**kwargs['source_formatter'])
        else:
            self.source_formatter = BaseFormatter()

        self.modifiers = []
        if 'data_modifiers' in kwargs and len(kwargs['data_modifiers']):
            for data_modifier in kwargs['data_modifiers']:
                if 'class' in data_modifier and len(data_modifier['class']):
                    self.modifiers.append(globals()[data_modifier['class']](**data_modifier))

        if 'destination_formatter' in kwargs and 'class' in kwargs['destination_formatter'] \
                and len(kwargs['destination_formatter']['class']):
            self.destination_formatter = \
                globals()[kwargs['destination_formatter']['class']](**kwargs['destination_formatter'])
        else:
            self.destination_formatter = BaseFormatter()

        self.destination = globals()[kwargs['destination']['class']](**kwargs['destination'])

    def execute(self):
        """
        Task execution method
        :return:
        """
        raise NotImplementedError()
