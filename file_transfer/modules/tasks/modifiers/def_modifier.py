from file_transfer.modules.tasks.modifiers import BaseModifier


class DefModifier(BaseModifier):
    """
    Definitive modifier class
    """
    def __init__(self, **kwargs):
        super(DefModifier, self).__init__()

    def modify(self, string_list: list[str], **kwargs) -> list[str]:
        """
        Modify file data
        :param string_list:
        :return:
        """
        result_lines = []
        for line in string_list:
            if line.startswith("Total for the day:"):
                elements = [el for el in line.split(' ') if len(el)]
                result_lines.append(f'{kwargs["mjd"]}.000000 {elements[6][0]}{elements[6][2:]}\n')
                break
        return result_lines
