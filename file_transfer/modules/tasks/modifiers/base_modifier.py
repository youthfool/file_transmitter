

class BaseModifier:
    """
    Base modifier class
    """
    def modify(self, string_list: list[str], **kwargs) -> list[str]:
        """
        Modify file data
        :param string_list:
        :return:
        """
        return string_list
