from file_transfer.modules.tasks.transfer_task import TransferTask
from file_transfer.tools.util import get_mjd_datetime
from datetime import datetime


class ConcatFileTransferTask(TransferTask):
    """
    Transfer task,
    which handles concatenation multiple source files into one destination file
    """

    def __init__(self, **kwargs):
        super(ConcatFileTransferTask, self).__init__(**kwargs)

    def execute(self):
        try:
            # get source files list
            source_objects_list = self.source.get_data_objects_list(self.source.address, self.source_filter.filter)
        except Exception as err:
            self.logger.log_error(self.name, f'Source error {str(err)}')
            return
        try:
            # get destination files list
            destination_objects_list = self.destination.get_data_objects_list(self.destination.address)
        except Exception as err:
            self.logger.log_error(self.name, f'Destination error {str(err)}')
            return
        try:
            # get destination files list with file names formatted to source files names
            destination_objects_formatted = self.destination_formatter.apply(destination_objects_list)
        except Exception as err:
            self.logger.log_error(self.name, f'Formatter error {str(err)}')
            return

        # keep source file names which are not in destination address
        source_objects_list = [file for file in source_objects_list if file not in destination_objects_formatted]

        mjds = {}

        for source_object in source_objects_list:
            try:
                mjd = get_mjd_datetime(datetime.strptime(source_object[-13:-5], '%Y%m%d'))

                # get data from source file
                data = self.source.get_data_from_data_objects(self.source.address, source_object)
                # apply modifiers to files data
                for modifier in self.modifiers:
                    data = modifier.modify(string_list=data, mjd=mjd)

                mjd_key = mjd + (4 - mjd % 5)
                if mjd_key in mjds:
                    mjds[mjd_key] += data
                else:
                    mjds[mjd_key] = data

            except Exception as err:
                self.logger.log_error(self.name, f'{source_object} handling error {err}')

        current_dt = datetime.utcnow()
        current_mjd = get_mjd_datetime(current_dt)
        for mjd in mjds:
            if mjd < current_mjd:
                # get new file name formatted from source file name to destination file name
                new_object_name = f'UTC(Km)_LOC_{mjd}_def.dat'
                try:
                    # send file to destination address
                    self.destination.send_data(self.destination.address, new_object_name, mjds[mjd])
                except Exception as err:
                    self.logger.log_error(self.name, f'{new_object_name} saving error {err}')
