from os import chdir
from os.path import abspath, dirname
import logging
import json
import sys
from typing import Tuple, List

from file_transfer.modules.tasks.task_factory import TransferTaskFactory
from file_transfer.modules.tasks.transfer_task import TransferTask
from modules.configurations import *
from modules.loggers import *
from file_transfer.modules.transfer_providers.transfer_providers import LocalTransferProvider, FtpTransferProvider
from file_transfer.modules.tasks.filters.base_filter import BaseFilter
from file_transfer.modules.tasks.filters.cggtts_file_filter import CGGTTSFileFilter


# set current working directory
abs_path = abspath(__file__)
dir_name = dirname(abs_path)
chdir(dir_name)


# set default logger
logging.basicConfig(filename='proc.log', level=logging.DEBUG,
                    format='%(asctime)s [%(levelname)s] %(message)s', datefmt='%d.%m.%Y %H:%M:%S')


def load_settings() -> dict:
    """
    Load config from json file format
    :return: dict with settings, supported format is in settings__example.py file
    """
    settings_path = 'settings.json'
    if len(sys.argv) > 1:
        settings_path = sys.argv[1]
    with open(settings_path, 'r') as settings_file:
        config_dict = json.load(settings_file)
    return config_dict


def get_from_dict(source: dict, key_path: Tuple[str, ...]):
    """
    Get value from dictionary, also when dictionary is deep (dict in dict in dict in dict...)
    :param source: source dictionary
    :param key_path: tuple of keys to go deep into dictionary and get final value
    :return: value from dictionary or None
    """
    if isinstance(source, dict) and len(key_path) > 0 and key_path[0] in source:
        if len(key_path) == 1:
            return source.get(key_path[0])
        else:
            return get_from_dict(source.get(key_path[0]), key_path[1:])
    else:
        raise KeyError('dictionary doesnt have the ' + str(key_path) + ' key')


def modify_cggtts_for_public(strings: List[str]) -> List[str]:
    """
    Modifier for cggtts files transfer task
    :param strings:
    :return:
    """
    for index, line in enumerate(strings):
        if line.startswith(('X =', 'Y =', 'Z =')):
            parts = line.split(' ')
            parts[2] = '0.0'
            strings[index] = ' '.join(parts)
    return strings


def main():
    # load base settings
    settings = load_settings()

    # prepare loggers
    logger_class = globals()[get_from_dict(settings, ("logger", "class",))]
    logger: ExecutionLogger = logger_class(**get_from_dict(settings, ("logger",)))

    # parse configurations with tasks details and create tasks
    config_class = globals()[get_from_dict(settings, ("config", "class",))]
    configurations: ConfigurationsProvider = config_class(**get_from_dict(settings, ("config",)),
                                                          logger=logger)

    # prepare tasks
    tasks_factory = TransferTaskFactory()
    tasks: list[TransferTask] = []
    for configuration in configurations.get_tasks_config_list():
        task: TransferTask = tasks_factory.create_task(task_config=configuration, logger=logger)
        tasks.append(task)

    # execute tasks
    for task in tasks:
        task.execute()

    # create report or notifications
    pass

    # send report or notify
    pass


def old_main():
    # configuration
    try:
        config = load_settings()

        if get_from_dict(config, ('source', 'is_ftp',)):
            source_tp = FtpTransferProvider(
                get_from_dict(config, ('source', 'path',)).encode('windows-1251').decode(),
                get_from_dict(config, ('source', 'server_address',)),
                get_from_dict(config, ('source', 'login',)),
                get_from_dict(config, ('source', 'password',))
            )
        else:
            source_tp = LocalTransferProvider(
                get_from_dict(config, ('source', 'path',)).encode('windows-1251').decode()
            )

        if get_from_dict(config, ('destination', 'is_ftp',)):
            destination_tp = FtpTransferProvider(
                get_from_dict(config, ('destination', 'path',)).encode('windows-1251').decode(),
                get_from_dict(config, ('destination', 'server_address',)),
                get_from_dict(config, ('destination', 'login',)),
                get_from_dict(config, ('destination', 'password',))
            )
        else:
            destination_tp = LocalTransferProvider(
                get_from_dict(config, ('destination', 'path',)).encode('windows-1251').decode()
            )

        filters = get_from_dict(config, ('filters',))
        extra_files = get_from_dict(config, ('extra_files',))

        if get_from_dict(filters, ('file_type',)) == 'cggtts':
            files_filter = CGGTTSFileFilter(pattern=get_from_dict(filters, ('file_mask',)),
                                            period=int(get_from_dict(filters, ('search_period',))))
        else:
            files_filter = BaseFilter()
    except Exception as err:
        logging.error("configuration error")
        logging.error(err)
        sys.exit()

    source_files = []
    destination_files = []

    # prepare files lists
    try:
        # get source files list
        source_files = source_tp.get_files_list(files_filter.filter)
        # add some extra files
        source_files += extra_files
        # delete doubles
        source_files = list(set(source_files))

        # get destination directory files list
        destination_files = destination_tp.get_files_list()

        # need only files, that are not in destination folder
        source_files = [file for file in source_files if file not in destination_files]
    except AttributeError as err:
        logging.error("error while preparing files list to transfer")
        logging.error(err)
        sys.exit()

    # transmit files
    if get_from_dict(filters, ('file_type',)) == 'cggtts':
        modifier = modify_cggtts_for_public
    else:
        modifier = lambda string_list: string_list
    fails = destination_tp.send_files(source_tp, source_files, modifier)

    # handle problems
    report = ""
    for fail in fails:
        report += fail[0] + ' failed with ' + str(fail[1]) + '\n'
    if len(report) > 0:
        logging.error(report)


if __name__ == '__main__':
    main()
