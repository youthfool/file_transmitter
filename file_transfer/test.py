import pathlib
import file_transfer.modules.configurations.local_json_config_provider

config_path = "task_config"


if pathlib.Path(config_path).is_file():
    print(config_path)
else:
    files = [f for f in pathlib.Path(config_path).glob("*.json") if not f.name.endswith("__example.json")]
    print(files)
    for file in files:
        with open(file, 'r') as r_file:
            print(r_file.readlines())


